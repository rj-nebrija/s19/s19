let num = 2
let getCube = num ** 3
console.log(`The cube of ${num} is ${getCube} `)

let address = ["222", "Sea Breeze Lane", "Brgy 87", "Tacloban City"]
let [houseNum, street, brgy, city ] = address

console.log(`I live at ${houseNum} ${street} ${brgy}, ${city}`)

let animal = {
	name : "Brownie",
	species : "Golden Retriever",
	weight :  "50 lbs",
	measurement : "4ft 2in"
}

let {name, species, weight, measurement} = animal

console.log(`${name} was a ${species}. He weighed at ${weight} with a measurement of ${measurement}.`)


let numArray = [1,2,10,25,33,65,23]

numArray.forEach((number) => {
	return console.log(number)
})

class Dog {
	constructor(name,age, breed) {
		this.name = name
		this.age = age
		this.breed = breed
	}
}

let husky = new Dog('Wolfie', 3, "Siberian Husky")

console.log(husky)